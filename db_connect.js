/* This script was written to insert, delete and extract data from MySQL
* Database in Nodejs */
// Importing the MySQL module
const mysql = require('mysql');


// Creating a connection
let conn = mysql.createConnection({
    // Setting the parameters for the mysql server
    host: '192.168.122.210',
    user: 'blackbox-server',
    password: '',
    database: 'fbi-database',
    insecureAuth: true
});


// Creating a function for connecting to the database and extracting
// Documents
function ExtractDataFromDatabase(input_data)
{
    // Connecting to the database
    conn.connect(function (error)
    {
        // Checking if there was an error in connection
        if (err) throw err;

        // If the connection was successful
        console.log("Connected to the database!");

        // Creating a variable to hold the sql queries
        let sql = "SELECT * FROM users WHERE FirstName = ?";

        // Sending the MySQL queries to the database
        conn.query(sql, [input_data], function(err, result)
        {
            // Checking for errors
            if (err) throw err;

            // If the connection to the database was a success
            console.log(result);

            // Closing up
            process.exit();
        });



    });
};
