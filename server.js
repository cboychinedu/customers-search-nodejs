// Importing the express module
const express = require('express');

// Importing the body-parser module
let bodyParser = require('body-parser');

/* Assuming we have the data */
let dataBaseData = {
    // Adding data to the database
    FirstName: "Mbonu",
    LastName: "Chinedum",
    Age: 21,
    Sex: "Male",
    Location: [0.9867, 24.90],
    PhoneNumber: "+234-098-3333-1",
    ImageUrl: "images/img.jpg"
};

// Getting the full path to the current working directory
const path = require('path');

// Defining the express application
const app = express();

// Using the body parser
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// Using the express router and making the process run on
// PORT: 3000
const router = express.Router();
const PORT = process.env.PORT || 3000;


/* FUNCTION SECTION */

function queryTheDatabase() {};

function extractData() {};


/* FUNCTION SECTION CLOSES */




// Serving the static assets
app.use(express.static('public'));

// Adding the first route for the '/' ==> GET request
app.get('/', (req, res, next) =>
{
    // Rendering the page 'index.html'
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Adding the Search page section 'GET'
app.get('/searchUser', (req, res, next) =>
{
    // Rendering the page 'searchUser.html'
    res.sendFile(path.join(__dirname, 'public', 'searchUser.html'));
});

// Adding the search page section 'POST'
app.post('/searchUser', (req, res, next) =>
{
    //
    console.log(req.body);
    console.log(req.body['searchValue']);

    //
    res.send(dataBaseData);
})

// Adding the home page section
app.get('/home', (req, res, next) =>
{
    // Rendering the page 'home.html'
    res.sendFile(path.join(__dirname, 'public', 'home.html'));
})


// Running the nodejs server
app.listen(PORT, 'localhost', () =>
{
    //
    console.log("Server running on 'localhost:3000'");
    console.log(`Server connected at: ${PORT}`);
});
