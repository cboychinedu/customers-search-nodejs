// Testing
console.log(`Working!`);

// Getting the search button html DOM element
let searchbtn = document.getElementById("search-btn");

// Creating an event listener for the button clicked
searchbtn.addEventListener("click", function()
{
    // Execute this block of code if the "search-btn" was clicked
    let searchValue = document.getElementById("search-text").value;
    console.log(searchValue);

    //
    connectToDB(searchValue);
});

//
document.querySelector("#search-text").addEventListener("keypress", function(event)
{
    //
    if ( event.key === "Enter" )
    {
        //
        let searchValue = document.getElementById("search-text").value;

        //
        connectToDB(searchValue);
    }

});


// USING AJAX TO CONNECT TO THE DATABASE//SERVER
function connectToDB(searchValue)
{
  // Getting the document elements by their respective id's
  let FirstName = document.getElementById("first-name-value");
  let LastName = document.getElementById("last-name-value");
  let Age = document.getElementById("age-value");
  let Sex = document.getElementById("sex-value");
  let Location = document.getElementById("location-value");
  let PhoneNumber = document.getElementById("phone-number-value");
  let ImageUrl = document.getElementById("image-url");


   // CODE EXECUTION
   $.ajax({
     // The connction type and the url
     type: 'POST',
     url: '/searchUser',

     // Sending the data
     crossDomain: true,
     dataType: "json",
     data: { searchValue: searchValue },

     // Checking for success
     // success: function(data)
     // {
     //     //
     //     alert("Success!");
     // }

  })
  .done(function (data)
  {
      // Execute this block of code if the connection is done and it returns
      // A value.
      if (data.error == true)
      {
          //
          alert("Error in connection.");
          console.log("Error in connection.");
      }

      // Else-block if the connection was sucessful
      else
      {
          //
          console.log(data);

          // Changing the values for the first name
          FirstName.style.marginLeft = "31px";
          FirstName.style.backgroundColor = "aqua";
          FirstName.style.paddingTop = "10px";
          FirstName.style.paddingBottom = "20px";
          FirstName.style.paddingLeft = "30px";
          FirstName.style.paddingRight = "30px";
          FirstName.style.borderRadius = "25px";
          FirstName.innerHTML = data["FirstName"];

          // Changing the values for the second name
          LastName.style.marginLeft = "40px";
          LastName.style.backgroundColor = "aqua";
          LastName.style.paddingTop = "10px";
          LastName.style.paddingBottom = "20px";
          LastName.style.paddingLeft = "30px";
          LastName.style.paddingRight = "30px";
          LastName.style.borderRadius = "25px";
          LastName.innerHTML = data["LastName"];

          // Changing the values for the age value
          Age.style.marginLeft = "96px";
          Age.style.backgroundColor = "aqua";
          Age.style.paddingTop = "10px";
          Age.style.paddingBottom = "20px";
          Age.style.paddingLeft = "30px";
          Age.style.paddingRight = "30px";
          Age.style.borderRadius = "25px";
          Age.innerHTML = data["Age"];

          // Changing the values for the sex value
          Sex.style.marginLeft = "98px";
          Sex.style.backgroundColor = "aqua";
          Sex.style.paddingTop = "10px";
          Sex.style.paddingBottom = "20px";
          Sex.style.paddingLeft = "30px";
          Sex.style.paddingRight = "30px";
          Sex.style.borderRadius = "25px";
          Sex.innerHTML = data["Sex"];

          // Changing the value for the location
          Location.style.marginLeft = "52px";
          Location.style.backgroundColor = "aqua";
          Location.style.paddingTop = "10px";
          Location.style.paddingBottom = "20px";
          Location.style.paddingLeft = "30px";
          Location.style.paddingRight = "30px";
          Location.style.borderRadius = "25px";
          Location.innerHTML = data["Location"];

          // Changing the values for the phone number
          PhoneNumber.style.marginLeft = "3px";
          PhoneNumber.style.backgroundColor = "aqua";
          PhoneNumber.style.paddingTop = "10px";
          PhoneNumber.style.paddingBottom = "11px";
          PhoneNumber.style.paddingLeft = "19px";
          PhoneNumber.style.paddingRight = "21px";
          PhoneNumber.style.borderRadius = "25px";
          PhoneNumber.innerHTML = data["PhoneNumber"];

          // Changing the value for the image url
          ImageUrl.style.marginLeft = "3px";
          ImageUrl.style.backgroundColor = "aqua";
          ImageUrl.style.paddingTop = "10px";
          ImageUrl.innerHTML = data["ImageUrl"];

          // Displaying the image
          // Getting the image sectio
          const imageSection = document.getElementById("image-section");
          imageSection.src = data["ImageUrl"];


      }
  });
};



// let ImageUrl = document.getElementById("image-url");
