//
console.log("Working!");

// Getting the login button by it's id value
let loginBtn = document.getElementById('login');

// Getting the email address input section
let emailAddressSection = document.getElementById('email');

// Getting the password input section
let passwordSection = document.getElementById('password');


// Adding an event listener for the login button
loginBtn.addEventListener("click", function()
{
  // Getting the email address
  let emailAddress = emailAddressSection.value;

  // Getting the password
  let password = passwordSection.value;

  // Execute this code if the login button was pressed
  console.log("login button pressed!");
  console.log(`The email address is: ${emailAddress}\n
    The password is: ${password} \n`);
});

// Adding an event listener for the email address section
emailAddressSection.addEventListener("click", function()
{
    // Execute this block of code if the emailAddress text field was clicked
    console.log("The email address text field was clicked!");
    emailAddressSection.style.paddingTop = "10px"; 

})
